class Partner extends HTMLElement {
  constructor() {
    super()

    this.innerHTML = `
      <style>
        #partner {
          display: flex;
          flex-direction: column;
          align-items: center;
        }

        #submit {
          height: 20px;
        }
      </style>

      <main id="partner">
        <h1>University Athlete</h1>

        <div>
          <h3>Sign Up Form</h3>
          Name:
          <br>
          <input type="text">
          <br>
          <br>
          Email:
          <br>
          <input type="text">
          <br>
          <br>

          <div>
            <h3>Interested in NCSA?</h3>
            Schedule a Time <input type="checkbox" id="ncsa-lead-submit" form-values="?name=John Smith&email=johnsmith@email.com">
          </div>
          <br>

          <button id="submit">Sign Up for UA!</button>
        </div>
      </main>
    `;
  }
}

export default Partner;
